# Importations d\'étiquettes pour gitlab via l'api

## Prérequis
- Avoir Python d'installé
- Avoir pip d'installé pour récupérer la bibliothèque `requests`

## Installation et utilisation
Assurez vous d'avoir `requests` sur votre machine  
`pip install requests`  
Remplissez les informations du repo dans le fichier scripts.py  
Définissez les étiquettes que vous souhaitez importer dans votre projet avec leurs couleurs  
Exécuter le script `python scripts.py`

## Informations
Pour une utilisation sur GITHUB, pensez à rajouter l'owner du repo et le repo avec son nom à la place de l'ID du projet.
Ces informations sont a passer dans l'url de la requête API.