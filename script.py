import requests
import json

# Configurations pour l'API GitLab
GITLAB_URL = "https://gitlab.com"  # Remplacez par l'URL de votre instance GitLab
PRIVATE_TOKEN = "XXXXXX"  # Remplacez par votre jeton d'accès privé GitLab
PROJECT_ID = "XXXXXX"  # Remplacez par l'ID du projet dans lequel vous souhaitez importer les étiquettes (se trouve dans les settings général de votre projet)
LABELS_FILE = "labels.json"  # Le chemin vers le fichier contenant les étiquettes

def create_labels():
    with open(LABELS_FILE, "r") as f:
        labels = json.load(f)

    headers = {
        "Private-Token": PRIVATE_TOKEN,
    }

    for label in labels:
        data = {
            "name": label["name"],
            "color": label["color"],
        }

        response = requests.post(
            f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/labels",
            headers=headers,
            data=data,
        )

        if response.status_code == 201:
            print(f"Étiquette '{label['name']}' créée avec succès.")
        else:
            print(
                f"Échec lors de la création de l'étiquette '{label['name']}': {response.text}"
            )


if __name__ == "__main__":
    create_labels()
